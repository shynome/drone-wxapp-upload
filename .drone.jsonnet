
local Cache(name, settings) = {
  name: name,
  image: 'drillster/drone-volume-cache',
  settings: settings,
  volumes: [
    { name: 'cache', path: '/cache' }
  ]
};

local Pipeline(name, trigger) = {
  kind: "pipeline",
  name: name,
  trigger: trigger,
  steps: [
    Cache('restore-cache', { mount: [ '_drone_tmp/docker' ], restore: true }),
    { name: 'prebuild',
      image: 'shynome/npm-build',
      commands: [
        'npm run set_tags'
      ]
    },
    { name: 'docker_publish',
      image: 'plugins/docker',
      settings: {
        repo: '${DRONE_REPO%%-docker}',
        dockerfile: 'Dockerfile',
        storage_path: '_drone_tmp/docker'
      },
      environment: {
        DOCKER_USERNAME: { from_secret: 'docker_username' },
        DOCKER_PASSWORD: { from_secret: 'docker_password' }
      }
    },
    Cache('rebuild-docker-cache', { mount: ['_drone_tmp/docker'], rebuild: true }),
    { name: 'report',
      image: 'shynome/alpine-drone-ci',
      when: { status: ['failure'] },
      environment: {
        REPORT_HOOK: { from_secret: 'report_hook' }
      },
      settings: { deploy: 'report' }
    }
  ],
  volumes: [
    {
      name: 'cache',
      host: { path: '/tmp/cache' },
    },
  ],
};

[
  Pipeline("dev",{ branch: [ 'dev' ], event: [ 'push' ] }),
  Pipeline("tag",{ event: [ 'tag' ] })
]
