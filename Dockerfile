# 精简了一些不必要的包
FROM canyoutle/wxdt@sha256:7c33d7ca0e9b201476b4f4ac1b52023ae945f3fb3e0723cb8e6653ddd74883e0
COPY rootfs /
RUN chmod +x /wxapp-ci/bin/*
ENTRYPOINT [ "/wxdt/bin/docker-entrypoint.sh" ]
CMD [ "/wxapp-ci/bin/wxapp-upload" ]
